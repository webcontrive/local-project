// Because of the way this file is set up, this works the same
// as a document.ready statement.
// Add your code, either within the misc: section, or create your own function
// if you create your own, make sure to add a line at the bottom to run that code
// ie: Engine.ui.misc();
$(function() {
    "use strict";
    var Engine = {
        ui: {
            stickyHeader: function() {
                $(window).scroll(function() {
                    if ($(this).scrollTop() >= 1) {
                        $('.primary_nav').addClass('stickyHeader');
                        $('.detail_breadcrumb').addClass('stickymove');
                    } else {
                        $('.primary_nav').removeClass('stickyHeader');
                        $('.detail_breadcrumb').removeClass('stickymove');
                    }
                });

            },
			 BGfillBox: function () {
            	$('.fill-box').fillBox();
       		 }, // end BG fill Box
			
			
            misc: function() {
				$(".btn-search,.btn-close-search").on('click', function () {
                $(".topheader").toggleClass("opensearch");
                return false;
            });
				
                $(".navbar-expand-toggle,.close-nav").on('click', function() {
                    $(".app-container").toggleClass("expanded");
                    return false;
                });
				
				 //  cart sidebar
				
				var SideContentHeight = $('.block-cart-header').innerHeight() + + $('.block-cart-footer').innerHeight();
            $('.cart-middle-items').css('height',$( window ).height() - SideContentHeight + 'px');
			
				$("#shoppingcart,.cart-close").on('click', function () {
                $("body").toggleClass("CartOpen");
                return false;
            });
			/*---------   Quantity Counter   ---------*/

            $('.plus').on('click', function () {
                var $qty = $(this).parents('.CartItem').find("ul.QuantityBox li .QuantityInput");
                var currentVal = parseInt($qty.val(), 10);
                if (!isNaN(currentVal)) {
                    $qty.val(currentVal + 1);
                }
            });
            $('.minus').on('click', function () {
                var $qty = $(this).parents('.CartItem').find("ul.QuantityBox li .QuantityInput");
                var currentVal = parseInt($qty.val(), 10);
                if (!isNaN(currentVal) && currentVal > 1) {
                    $qty.val(currentVal - 1);
                }
            });
		/*---------   Hero Slider   ---------*/
		
		var heroslideheight = $('header').innerHeight();
            $('ul.Homeslider li').css('max-height',$( window ).height() - heroslideheight + 'px');
		

            }, // end misc
            metisNavMenu: function() {
                $("#side-menu").metisMenu();
            }, // end metisMenu

        }, // end ui
        utils: {
            sliders: function() {
                //  Home Slider
                $('.Homeslider').slick({
                    arrows: false,
                    speed: 1000,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                    touchMove: true,
                    slide: 'li'
                });
                
            },
        }, // end utils
    };



    Engine.ui.misc();
    Engine.ui.metisNavMenu();
    //Engine.ui.stickyHeader();
	Engine.ui.BGfillBox();	
    Engine.utils.sliders();


});

$(document).mouseup(function(e) {
    var popup = $(".side-menu.sidebar-inverse");
    if (!$('.navbar-expand-toggle').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
        if ($(".app-container").hasClass("expanded")) {
            $(".close-nav").trigger("click");
        }
    }
});